package com.example.student.snapchat;

public class Constants {
    public static final String  ACTION_ADD_FRIEND = "com.example.student.snapchat.ADD_FRIEND";

    public static final String  BROADCAST_ADD_FRIEND_SUCCESS = "com.example.student.snapchat.ADD_FRIEND_SUCCESS";
    public static final String  BROADCAST_ADD_FRIEND_FAILURE = "com.example.student.snapchat.ADD_FRIEND_FAILURE";

}
