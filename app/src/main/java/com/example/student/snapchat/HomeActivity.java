package com.example.student.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.backendless.Backendless;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        HomeMenuFragment homeMenu = new HomeMenuFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, homeMenu).commit();

    }
}
