package com.example.student.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class AddFriendsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_friends);

        AddFriendsFragment addfriends = new AddFriendsFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.addfriendsContainer, addfriends).commit();
    }
}
