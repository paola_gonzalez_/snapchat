package com.example.student.snapchat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.backendless.Backendless;

public class MainActivity extends AppCompatActivity {

    public static final String APP_ID = "DC04B36F-BE2B-EF76-FFB5-E769AEA4D500";
    public static final String SECRET_KEY = "75242794-F920-F69F-FFE5-05FAE98E1900";
    public static final String VERSION = "v1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Backendless.initApp(this, APP_ID, SECRET_KEY, VERSION);
        if (Backendless.UserService.loggedInUser() == ""){
            MainMenuFragment mainMenu = new MainMenuFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, mainMenu).commit();
        }else{
            HomeMenuFragment homeMenu = new HomeMenuFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, homeMenu).commit();
        }
    }


}
